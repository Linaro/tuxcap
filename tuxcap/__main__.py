#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2022-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import argparse
import os
from pathlib import Path
import subprocess
import sys
import tempfile

import json


##########
# Setups #
##########
def setup_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog="tuxcap", description="Tux terminal capture")

    parser.add_argument("--version", action="version", version=f"%(prog)s, 0.1")
    parser.add_argument("--edit", action="store_true", help="Edit the file")

    parser.add_argument("output", help="output file")
    return parser


###########
# Helpers #
###########
def cleancast(data):
    remaining = ""
    for line in data.split("\n")[0:-2]:
        data = json.loads(line)
        if "version" in data and "timestamp" in data:
            yield data
            continue
        assert data[1] == "o"
        data[2] = remaining + data[2]
        remaining = ""
        if "\r\n" in data[2]:
            (data[2], remaining) = data[2].split("\r\n", 1)
            data[2] += "\r\n"
        yield data

    while remaining:
        data[2] = remaining
        remaining = ""
        if "\r\n" in data[2]:
            (data[2], remaining) = data[2].split("\r\n", 1)
            data[2] += "\r\n"
        yield data


def makefast(data):
    time = 0.1
    for line in data:
        if "version" in line and "timestamp" in line:
            # Leave first line unmodified
            yield json.dumps(line) + "\n"
            continue
        line[0] = time
        yield json.dumps(line) + "\n"
        time += 0.05
        if len(line[2]) > 5:
            # Increase delay time after a multi-char line
            time += 0.5

    # Pause at end
    for delay in [0, 0.5, 1, 1.5, 2]:
        yield json.dumps([time + delay, "o", ""]) + "\n"


##############
# Entrypoint #
##############
def main() -> int:
    # Parse command line
    parser = setup_parser()
    options = parser.parse_args()

    with tempfile.NamedTemporaryFile(suffix=".cast") as f_cast:
        subprocess.run(
            [
                "asciinema",
                "rec",
                "--command",
                "bash --norc",
                "--overwrite",
                f_cast.name,
            ],
            env={**os.environ, "PS1": "\e[00;32m$\e[00;37m "},
        )
        data = Path(f_cast.name).read_text(encoding="utf-8")

    with tempfile.NamedTemporaryFile(suffix=".cast", mode="w") as f_cast:
        if options.edit:
            with tempfile.NamedTemporaryFile(suffix=".cast", mode="w") as f_cleancast:
                for d in cleancast(data):
                    f_cleancast.write(json.dumps(d) + "\n")
                f_cleancast.flush()
                subprocess.run([os.environ.get("EDITOR"), f_cleancast.name])
                data = (
                    json.loads(l)
                    for l in Path(f_cleancast.name)
                    .read_text(encoding="utf-8")
                    .split("\n")
                    if l
                )
        else:
            data = cleancast(data)
        f_cast.write("".join(makefast(data)))
        f_cast.flush()
        subprocess.run(["termtosvg", "render", f_cast.name, options.output])
    return 0


if __name__ == "__main__":
    sys.exit(main())
